extends "res://logic/SailingEvent/AbstractSailingEvent.gd"


var action 


func begin_event():
	if self.action == "start":
		self.level.get_node("Environment").start_weather()
	elif self.action == "stop":
		self.level.get_node("Environment").stop_weather()
