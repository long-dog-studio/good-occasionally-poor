extends "res://logic/SailingEvent/AbstractSailingEvent.gd"


const ship_scene = "res://levels/SailingLevel/SunkenShip/SunkenShip.tscn"
var ship_interval_duration = 5
var ship_interval_timer = Timer.new()

# Yes, the code for this is basically like the seaweed and the rock ones and ideally can be combined.
# But it is 2 am and not much time left for that sort of thing.
func begin_event():
	print("%s: doing my thing (evil ships)" % self.name)
	ship_interval_timer.one_shot = false
	ship_interval_timer.connect("timeout", self, "_on_ShipIntervalTimer_timeout")
	self.add_child(ship_interval_timer)
	ship_interval_timer.start(self.ship_interval_duration)

func end_event():
	ship_interval_timer.disconnect("timeout", self, "_on_ShipIntervalTimer_timeout")
	.end_event()

func spawn_ship():
	var spawn_location = Vector2(self.level.resolution.x, self.level.sea_extent_y + randi() % (int(self.level.resolution.y) - self.level.sea_extent_y))
	self.level.spawn_scene_at_position(preload(ship_scene).instance(), spawn_location)

func _on_ShipIntervalTimer_timeout():
	self.spawn_ship()
