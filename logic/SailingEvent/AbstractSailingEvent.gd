extends Node


# reference to the level, so we may control the goings on
var level

# duration of whole event in seconds (real-time)
var duration

var is_night = false

signal complete


func _ready():
	var event_timer = Timer.new()
	event_timer.one_shot = true
	event_timer.connect("timeout", self, "_on_EventTimer_timeout")
	self.add_child(event_timer)
	event_timer.start(duration)

	self.begin_event()

func begin_event():
	print("%s: doing my thing (nothing)" % self.name)

func end_event():
	print("%s: ending event." % self.name)

func _on_EventTimer_timeout():
	end_event()
	emit_signal("complete")

