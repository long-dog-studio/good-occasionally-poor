extends Area2D


var damage = 1
var speed = 50
var is_night

func _ready():
	self.play_animation("default")
	var _area_conn = self.connect("area_entered", self, "_on_Area2D_area_entered")

func _process(delta):
	if self.position.x < 0:
		self.queue_free()

	self.position.x -= speed * delta

func set_night(value):
	if value:
		is_night = true
		self.play_animation($AnimatedSprite.animation)

func play_animation(animation):

	animation = animation.replace("night-", "")
	

	if self.is_night:
		$AnimatedSprite.play("night-" + animation)
	else:
		$AnimatedSprite.play(animation)
	

func _on_Area2D_area_entered(area):
	if area.is_in_group("player"):
		area.damage(damage, self)
