extends AnimatedSprite


func _ready():
	pass

func start():
	self.visible = true
	$AudioStreamPlayer.play()

func stop():
	self.visible = false
	$AudioStreamPlayer.stop()
