extends Node2D


var resolution = Vector2(ProjectSettings.get_setting("display/window/size/width"), ProjectSettings.get_setting("display/window/size/height"))
const sea_extent_y = 80

var waypoints

var current_waypoint = null
var current_event = null

var time_elapsed = 0
var time_allowed

var skip_intro

signal boat_destroyed
signal show_dialog
signal fish_caught
signal change_level
signal time_elapsed
signal night_time_reached

var is_night 

var dialog_events = [
	"fishing"
]


func _ready():
	self.set_night(is_night)
	$Boat.set_docked = false 
	$Environment.spawn_mountain()
	
	$TutorialTimer.start(2)

	var _boat_destroyed_conn = $Boat.connect("destroyed", self, "_on_Boat_destroyed")
	var _tutc = $TutorialTimer.connect("timeout", self, "_on_TutorialTimer_timeout")

func start(game):
	self.skip_intro = game.skip_intro
	self.time_allowed = game.time_allowed

	var waypoint_head = game.route_data
	self.waypoints = self.waypoint_linked_list_to_waypoint_array(waypoint_head)
	self.next_waypoint()
	self.next_event()
	self.start_event()

func waypoint_linked_list_to_waypoint_array(waypoint_head):
	var _waypoints = []
	var waypoint = waypoint_head
	while waypoint != null:
		_waypoints.append(waypoint)
		waypoint = waypoint.next_waypoint

	return _waypoints

func start_event():
	if self.current_event:
		self.current_event.level = self
		self.current_event.is_night = self.is_night
		self.add_child(self.current_event)
		var _c = self.current_event.connect("complete", self, "_on_SailingEvent_complete")
	
func stop_event():
	self.current_event.disconnect("complete", self, "_on_SailingEvent_complete")

func next_event():
	self.current_event = self.current_waypoint.events.pop_front()
	if !self.current_event:
		print("%s: no more events for this waypoint." % [self.name])
		self.next_waypoint()

		if self.current_waypoint:
			self.current_event = self.current_waypoint.events.pop_front()
		else:
			print("%s: no more waypoints or events." % [self.name]) 
			return null

func next_waypoint():
	if self.current_waypoint: 
		self.update_time(self.current_waypoint.time_cost)
	self.current_waypoint = self.waypoints.pop_front()		

func spawn_scene_at_position(scene, position, with_name=null):
	scene.set_night(is_night)
	if with_name:
		scene.name = with_name
	scene.add_to_group("spawned")
	scene.set_position(position)
	self.add_child(scene)

func update_time(time_to_add):
	self.time_elapsed += time_to_add
	print("%s: %s hours elapsed" % [self.name, self.time_elapsed])
	emit_signal("time_elapsed", self.time_elapsed)
	if self.time_elapsed >=  self.time_allowed / 2 && !self.is_night:
		emit_signal("night_time_reached")
		self.set_night(true)

func set_night(value):
	if value:
		self.is_night = true
		$Boat.set_night(true)
		$Environment.set_night(true)
		$BackgroundAnimatedSprite.sunset()
		for object in get_children():
			if object.is_in_group("spawned"):
				object.set_night(true)

func connect_level_signals(game):
	var _c = self.connect("boat_destroyed", game, "_on_Level_boat_destroyed")
	var _fc = self.connect("fish_caught", game, "_on_Level_fish_caught")
	var _dialog_c = self.connect("show_dialog", game, "_on_Level_show_dialog")
	var _tutorial_c = self.connect("tutorial_finished", game, "_on_Level_tutorial_finished")
	var _change_level_c = self.connect("change_level", game, "_on_Level_change_level")
	var _time_elapsed_c = self.connect("time_elapsed", game, "_on_Level_time_elapsed")
	var _night_time_reached_c = self.connect("night_time_reached", game, "_on_Level_night_time_reached")

func _on_SailingEvent_complete():
	self.stop_event()
	self.next_event()
	self.start_event()

func _on_Boat_destroyed():
	self.update_time(2)
	emit_signal("boat_destroyed")

func _on_Shoal_fish_caught(type):
	$FishCaughtAudioStreamPlayer.play()
	emit_signal('fish_caught', type)

func _on_TutorialTimer_timeout():
	var dialog_event = self.dialog_events.pop_front()
	print("%s: Next dialog is %s" % [self.name, dialog_event])

	if dialog_event != null && !self.skip_intro:
		emit_signal("show_dialog", "fish_tutorial", dialog_event)
	
	else:
		$TutorialTimer.stop()
		emit_signal("tutorial_finished")

func change_level(level_id):
	emit_signal("change_level", level_id, "default")
