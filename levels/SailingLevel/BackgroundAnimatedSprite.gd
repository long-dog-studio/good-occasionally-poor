extends AnimatedSprite


func sunset():
	self.play("sunset")
	var _c = self.connect("animation_finished", self, "_on_sunset_animation_finished")

func _on_sunset_animation_finished():
	self.disconnect("animation_finished", self, "_on_sunset_animation_finished")
	self.play("night-default")
