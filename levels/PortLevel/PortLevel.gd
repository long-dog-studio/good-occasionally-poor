extends Node2D


signal show_dialog
signal show_book
signal show_map
signal show_list
signal title_drop
signal tutorial_finished

var is_night 
var skip_intro

var dialog_events = [
	"move",
	"order",
	"bestiary",
	"objective",
	"map"
]


func _ready():
	$Environment.enable_mountain_spawning(false)
	$Boat.is_night = is_night
	$Boat.set_docked(true)

	if is_night:
		$Harbour.play("night-default")
		$AmbientBoats.visible = false
	else:
		$Harbour.play('default')

	$FadeInTimer.start(15)

	var _fc = $FadeInTimer.connect("timeout", self, "_on_FadeInTimer_timeout")
	var _tdc = $TitleDropTimer.connect("timeout", self, "_on_TitleDropTimer_timeout")
	var _tutc = $TutorialTimer.connect("timeout", self, "_on_TutorialTimer_timeout")
	
func start(game):
	self.skip_intro = game.skip_intro

func _process(_delta):
	if Input.is_action_just_pressed("ui_book"):
		emit_signal("show_book")

	if Input.is_action_just_pressed("ui_map"):
		emit_signal("show_map")	

	if Input.is_action_just_pressed("ui_list"):
		emit_signal("show_list")	
	
func connect_level_signals(game):
	var _book_c = self.connect("show_book", game, "_on_Level_show_book")
	var _map_c = self.connect("show_map", game, "_on_Level_show_map")
	var _list_c = self.connect("show_list", game, "_on_Level_show_list")
	var _dialog_c = self.connect("show_dialog", game, "_on_Level_show_dialog")
	var _tutorial_c = self.connect("tutorial_finished", game, "_on_Level_tutorial_finished")

func _on_FadeInTimer_timeout():
	emit_signal("title_drop")
	set_title_visibility(true)	
	$TitleDropTimer.start(10)

func _on_TitleDropTimer_timeout():
	set_title_visibility(false)
	$ShippingForecastAudioStreamPlayer2D/AnimationPlayer.play('fade_low')
	emit_signal("tutorial_finished")  # now misnamed
	$TutorialTimer.start(10)

func _on_TutorialTimer_timeout():
	var dialog_event = self.dialog_events.pop_front()
	print("%s: Next dialog is %s" % [self.name, dialog_event])

	if dialog_event != null && !self.skip_intro:
		emit_signal("show_dialog", "port_tutorial", dialog_event)

	else:
		$TutorialTimer.stop()

func set_title_visibility(visible):
	# do nothing if 'skip intro' is set
	if !self.skip_intro:
		if visible:
			$TitleRichTextLabel.fade_in()
		else:
			$TitleRichTextLabel.fade_out()
	