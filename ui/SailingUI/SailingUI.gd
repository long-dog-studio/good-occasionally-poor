extends "res://ui/AbstractUI/AbstractUI.gd"


func _ready():
	var _c = $TextPersistTimer.connect("timeout", self, "_on_TextPersistTimer_timeout")

func start(game):
	pass

func connect_ui_signals(game):
	var time_updated_signal = game.connect("time_updated", self, "_on_Game_time_updated") 

func set_time_remaining(time):
	var text = "%s hours until market" % str(max(time, 0))
	$TimeRichTextLabel.set_text(text)
	$AnimationPlayer.play("fade_in")
	$TextPersistTimer.start()

func _on_TextPersistTimer_timeout():
	$AnimationPlayer.play("fade_out")

func _on_Game_time_updated(time):
	self.set_time_remaining(time)
