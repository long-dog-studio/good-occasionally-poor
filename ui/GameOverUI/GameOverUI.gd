extends "res://ui/AbstractUI/AbstractUI.gd"


signal exit_game
signal restart


func _ready():
	pass

func start(game):
	var lines = []
	lines.append("[center]~ Day %s ~[/center]\n" % game.day)
	lines.append("Savings: £%s\n" % game.economy.total_money)
	lines.append("\nYou caught...\n")
	
	for fish_type in game.economy.fish_values.keys():
		lines.append("%s %s (£%s)\n" % [game.economy.count_catches_of_type(fish_type), fish_type, game.economy.evaluate_catches_of_type(fish_type)])

	if game.exceeded_time_limit():
		lines.append("\nYou missed the market and sold no fish!")
	else:
		lines.append("\nYou made £%s at the market!" % game.economy.evaluate_haul(game.exceeded_time_limit()))
	
	var text = ""
	for line in lines:
		text += line

	$RichTextLabel.bbcode_text = text

func _process(_delta):
	if Input.is_action_just_pressed('ui_accept'):
		emit_signal('restart')

	if Input.is_action_just_pressed('ui_cancel'):
		emit_signal('exit_game')

func connect_ui_signals(game):
	var _c = self.connect("exit_game", game, "_on_UI_exit_game_command") 
	var __c = self.connect("restart", game, "_on_UI_restart_command") 
