extends Node2D


var next_waypoint
var events = []
var time_cost = 0

# associates a SailingEvent with this waypoint, which will take place when the waypoint is reached.
# multiple events can be added.
func add_event(type, real_time_duration_seconds=60, params={}):
	var event_instance

	if type == 'null':
		event_instance = preload('res://logic/SailingEvent/AbstractSailingEvent.gd').new()
		event_instance.name = "NullSailingEvent"

	elif type == 'rocks':
		event_instance = preload('res://logic/SailingEvent/RockHazardEvent.gd').new()
		event_instance.name = "RockHazardEvent"
	
	elif type == 'seaweed':
		event_instance = preload('res://logic/SailingEvent/SeaweedHazardEvent.gd').new()
		event_instance.name = "SeaweedHazardEvent"

	elif type == 'ship':
		event_instance = preload('res://logic/SailingEvent/ShipHazardEvent.gd').new()
		event_instance.name = "ShipHazardEvent"

	elif type == 'fish':
		event_instance = preload('res://logic/SailingEvent/FishyEvent.gd').new()
		event_instance.name = "FishyEvent"
		event_instance.type = params['type']
		event_instance.surface_time = params['surface_time']

	elif type == 'weather_start':
		event_instance = preload('res://logic/SailingEvent/WeatherEvent.gd').new()
		event_instance.name = "WeatherStartEvent"
		event_instance.action = 'start'

	elif type == 'weather_stop':
		event_instance = preload('res://logic/SailingEvent/WeatherEvent.gd').new()
		event_instance.name = "WeatherStopEvent"
		event_instance.action = 'stop'

	elif type == 'return_to_port':
		event_instance = preload('res://logic/SailingEvent/ReturnToPortEvent.gd').new()
		event_instance.name = "ReturnToPortEvent"

	else: 
		assert(false)


	event_instance.duration = real_time_duration_seconds

	# these must be set later
	event_instance.level = null 
	event_instance.is_night = null

	self.events.append(event_instance)
